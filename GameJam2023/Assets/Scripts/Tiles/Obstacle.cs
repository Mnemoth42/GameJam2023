﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Jam23.Tiles
{
    public class Obstacle : MonoBehaviour
    {
        private static Dictionary<Vector2Int, Obstacle> obstacles = new Dictionary<Vector2Int, Obstacle>();

        private void Start()
        {
            obstacles[transform.position.ToInt()] = this;
        }
        
        private void OnDestroy()
        {
            obstacles[transform.position.ToInt()] = null;
        }

        public static Obstacle GetObstacle(Vector2 position)
        {
            if (obstacles.ContainsKey(position.ToInt())) return obstacles[position.ToInt()];
            return null;
        }
    }

    public static class TileHelpers
    {
        public static Vector2Int ToInt(this Vector2 position)
        {
            return new Vector2Int(Mathf.RoundToInt(position.x), Mathf.RoundToInt(position.y));
        }
        public static Vector2Int ToInt(this Vector3 position)
        {
            return new Vector2Int(Mathf.RoundToInt(position.x), Mathf.RoundToInt(position.y));
        }
    }
}