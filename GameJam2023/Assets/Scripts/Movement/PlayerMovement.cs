using System;
using System.Collections;
using InputAssets;
using Jam23.Tiles;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Jam23.Movement
{
    public class PlayerMovement : MonoBehaviour, PlayerControls.IMovementActions
    {
        [SerializeField] private float moveSpeed = .3f;
        private bool isMoving = false;

        private PlayerControls controls;
        
        private void Awake()
        {
            controls = new PlayerControls();
            controls.Movement.SetCallbacks(this);
        }

        private void OnEnable()
        {
            controls.Enable();
        }

        private void OnDisable()
        {
            controls.Disable();
        }

        private void OnDestroy()
        {
            controls.Dispose();
        }

        void AttemptMove(Vector2 direction)
        {
            if (isMoving) return;
            Vector2 target = transform.position;
            target += direction;
            if (Obstacle.GetObstacle(target)) return;
            StartCoroutine(MoveCoroutine(target));
        }

        private IEnumerator MoveCoroutine(Vector2 targetPosition)
        {
            Vector2 startPosition = transform.position;
            isMoving = true;
            float elapsed = 0;
            while (elapsed < 1)
            {
                elapsed += Time.deltaTime * moveSpeed;
                transform.position = Vector2.Lerp(startPosition, targetPosition, elapsed);
                yield return null;
            }
            transform.position = targetPosition;
            isMoving = false;
        }

        /// <inheritdoc />
        public void OnUp(InputAction.CallbackContext context)
        {
            if (context.performed)
            {
                AttemptMove(Vector2.up);
            }
        }

        /// <inheritdoc />
        public void OnDown(InputAction.CallbackContext context)
        {
            if (context.performed)
            {
                AttemptMove(Vector2.down);
            }
        }

        /// <inheritdoc />
        public void OnLeft(InputAction.CallbackContext context)
        {
            if (context.performed)
            {
                AttemptMove(Vector2.left);
            }
        }

        /// <inheritdoc />
        public void OnRight(InputAction.CallbackContext context)
        {
            if (context.performed)
            {
                AttemptMove(Vector2.right);
            }
        }
    }
}
